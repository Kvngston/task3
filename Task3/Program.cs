﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task3.DB;

namespace Task3
{
    class Program
    {
        static void Main(string[] args) 
        {
            MongoDb.connect();
            App.start();
        } 
    }
}
