﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task3.Models;

namespace Task3.Services
{
    interface BankingOperations
    {
        void withdraw(Customer customer);
        void checkBalance(Customer customer);
        void deposit(Customer customer);
        void transfer(Customer customer);
        void openAccount(string firstName, string lastName, string username, string password, int age);
        Customer login(string username, string password);

        List<Customer> getAllCustomers();
        void getAllUserTransactions(Customer customer);
        void getAllTransactions();
    }

}
