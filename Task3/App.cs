﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task3.Models;
using Task3.ServiceImpl;
using Task3.Services;

namespace Task3
{
    public class App
    {
        static BankingOperations bankingOperations = new BankingOperationsImpl();
        public static void start()
        {

            Console.WriteLine("Welcome to Tk's Banking System");
            Console.WriteLine("Please Choose an option");
            Console.WriteLine("1).  OPEN AN ACCOUNT");
            Console.WriteLine("2).  LOGIN");
            Console.WriteLine("3).  SHOW ALL TRANSACTIONS \n \n \n");
            var answer = Console.ReadLine();

            switch (answer)
            {
                case "1":
                    {
                        try
                        {
                            Console.WriteLine("Enter your FirstName");
                            var firstName = Console.ReadLine();
                            Console.WriteLine("Enter your LastName");
                            var lastName = Console.ReadLine();
                            Console.WriteLine("Enter your Age");
                            var age = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Enter your Username");
                            var username = Console.ReadLine();
                            Console.WriteLine("Enter your Password");
                            var password = Console.ReadLine();
                            bankingOperations.openAccount(firstName, lastName, username, password, age);
                                
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            start();
                        }
                            
                        break;
                    }
                case "2":
                    {
                        Console.WriteLine("Enter username");
                        var username = Console.ReadLine();
                        Console.WriteLine("Enter password");
                        var password = Console.ReadLine();
                        var result = bankingOperations.login(username, password);
                        Console.WriteLine("\n \n \n");

                        if (result == null)
                        {
                            start();
                        }else
                        {
                            MainInterface(result);
                        }
                        
                        break;
                    }
                case "3":
                    {
                        bankingOperations.getAllTransactions();
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Incorrect Input!, try again");
                        start();
                        break;
                    }
            }

        }

        public static void MainInterface(Customer customer)
        {
            Console.WriteLine("Welcome to Tk's Banking System");
            Console.WriteLine("Please Choose an option");
            Console.WriteLine("1).  WITHDRAW");
            Console.WriteLine("2).  DEPOSIT");
            Console.WriteLine("3).  Transfer");
            Console.WriteLine("4).  CheckBalance");
            Console.WriteLine("5).  Show Transaction History \n \n \n");

            var answer = Console.ReadLine();

                switch (answer)
                {
                    case "1":
                        {
                            bankingOperations.withdraw(customer);
                            Console.WriteLine("Do you want to perform another transaction?");
                            Console.WriteLine("Reply Y/N");
                            var input = Console.ReadLine().ToUpper();

                            if (input.Equals("Y"))
                            {
                                MainInterface(customer);
                            }else if (input.Equals("N"))
                            {
                                break;
                            }else
                            {
                                break;
                            }
                            break;
                        }
                    case "2":
                        {
                            bankingOperations.deposit(customer); 
                            Console.WriteLine("Do you want to perform another transaction?");
                            Console.WriteLine("Reply Y/N");
                            var input = Console.ReadLine().ToUpper();
                            switch (input)
                            {
                                case "Y":
                                    {
                                        MainInterface(customer);
                                        break;
                                    }
                                case "N":
                                    {
                                        break;
                                    }
                                default:
                                    {
                                        break;
                                    }
                            }
                            break;
                        }
                    case "3":
                        {
                            bankingOperations.transfer(customer);
                            Console.WriteLine("Do you want to perform another transaction?");
                            Console.WriteLine("Reply Y/N");
                            var input = Console.ReadLine().ToUpper();
                            switch (input)
                            {
                                case "Y":
                                    {
                                        MainInterface(customer);
                                        break;
                                    }
                                case "N":
                                    {
                                        break;
                                    }
                                default:
                                    {
                                        break;
                                    }
                            }
                            break;
                        }
                    case "4":
                        {
                            bankingOperations.checkBalance(customer);
                            Console.WriteLine("Do you want to perform another transaction?");
                            Console.WriteLine("Reply Y/N");
                            var input = Console.ReadLine().ToUpper();
                            switch (input)
                            {
                                case "Y":
                                    {
                                        MainInterface(customer);
                                        break;
                                    }
                                case "N":
                                    {
                                        break;
                                    }
                                default:
                                    {
                                        break;
                                    }
                            }
                            break;
                        }
                case "5":
                    {
                        bankingOperations.getAllUserTransactions(customer);
                        Console.WriteLine("Do you want to perform another transaction?");
                        Console.WriteLine("Reply Y/N");
                        var input = Console.ReadLine().ToUpper();
                        switch (input)
                        {
                            case "Y":
                                {
                                    MainInterface(customer);
                                    break;
                                }
                            case "N":
                                {
                                    break;
                                }
                            default:
                                {
                                    break;
                                }
                        }
                        break;
                    }
                default:
                        {
                            Console.WriteLine("Invalid Input");
                            MainInterface(customer);
                            break;
                        }
                }
        }
    }
}
