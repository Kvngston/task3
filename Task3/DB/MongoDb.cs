﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Task3.Models;

namespace Task3.DB
{
    public class MongoDb
    {

        public static IMongoDatabase connect()
        {
            //var connectionString = "mongodb+srv://kvngston:<>@bankapp.fuwhr.gcp.mongodb.net/<>?retryWrites=true&w=majority";

            MongoClient dbClient = new MongoClient("mongodb://localhost:27017");

            return dbClient.GetDatabase("BankApp");
            
        }
        public static IMongoCollection<Customer> CustomerModel()
        {
            var database = connect();
            return database.GetCollection<Customer>("Customer");
        }

        public static IMongoCollection<Transaction> TransactionModel()
        {
            var database = connect();
            return database.GetCollection<Transaction>("Transaction");
        }


    }
}
