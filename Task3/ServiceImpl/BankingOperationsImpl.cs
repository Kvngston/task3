﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using Task3.DB;
using Task3.Models;
using Task3.Services;

namespace Task3.ServiceImpl
{
    class BankingOperationsImpl : BankingOperations
    {
        public void checkBalance(Customer customer)
        {
            Console.WriteLine($"Your account balance is {"$"+customer.accountBalance}");
        }

        public void deposit(Customer customer)
        {
            Console.WriteLine("How much do you want to deposit?");
            try
            {
                var amount = Convert.ToDouble(Console.ReadLine());

                
                Transaction transaction = new Transaction();
                transaction.amount = amount;
                transaction.transactionType = Enums.TransactionType.DEPOSIT;
                transaction.receiverAccountNumber = customer.accountNumber;
                transaction.receiverName = customer.accountName;

                customer.accountBalance += amount;
                customer.transactions.Add(transaction);

                var transactionCollection = MongoDb.TransactionModel();
                var customerCollection = MongoDb.CustomerModel();
                var filter = Builders<Customer>.Filter.Eq("username", customer.username);
                transactionCollection.InsertOneAsync(transaction);
                customerCollection.FindOneAndReplace(filter, customer);

                Console.WriteLine($"You have Deposited {"$" + amount}");
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                withdraw(customer);
            }
        }

        public List<Customer> getAllCustomers()
        {
            var customerCollection = MongoDb.CustomerModel();

            return customerCollection.AsQueryable().ToList();


        }

        public void getAllTransactions()
        {
            var collection = MongoDb.TransactionModel();

            var transactions =  collection.AsQueryable().ToList();

            transactions.ForEach(transaction =>
            {
                Console.WriteLine(transaction.ToString());
            });

            Console.ReadLine();

        }

        public void getAllUserTransactions(Customer customer)
        {

            var collection = MongoDb.CustomerModel();
            var filter = Builders<Customer>.Filter.Eq("username", customer.username);
            var user = collection.Find(filter).FirstOrDefault();

            user.userTransactions();

        }

        public Customer login(string username, string password)
        {
            var collection = MongoDb.CustomerModel();
            var filter = Builders<Customer>.Filter.Eq("username", username);
            var result = collection.FindSync<Customer>(filter).FirstOrDefault();

            if(result == null)
            {
                Console.WriteLine("User not found");
                return null;
            }

            if (BCrypt.Net.BCrypt.Verify(password, result.password))
            {
                return result;
            }else
            {
                Console.WriteLine("Password Incorrect!");
                return null;
            }

        }

        public void openAccount(string firstName, string lastName, string username, string password, int age)
        {

            try
            {
                   
                Customer customer = new Customer()
                {
                    firstName = firstName,
                    lastName = lastName,
                    username = username,
                    password = BCrypt.Net.BCrypt.HashPassword(password),
                    age = age
                };


                customer.accountName = $"{lastName} {firstName}";
                customer.transactions = new List<Transaction>();

                var collection = MongoDb.CustomerModel();
                collection.InsertOneAsync(customer);
                Console.WriteLine("Account Created");

                Console.WriteLine("\n \n \n");
                App.MainInterface(customer);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                App.start();
            }
            
        }

        public void transfer(Customer customer)
        {
            try
            {
                Console.WriteLine("Enter Receipient Account Number");
                var accountNumber = Console.ReadLine();

                Console.WriteLine("Enter Amount");
                var amount = Convert.ToInt32(Console.ReadLine());

                if(customer.accountBalance < amount)
                {
                    Console.WriteLine("Insufficient Funds");
                    return;
                }

                if(accountNumber.Length < 9)
                {
                    Console.WriteLine("Invalid Account Number");
                    transfer(customer);
                }

                var customerCollection = MongoDb.CustomerModel();
                var transactionCollection = MongoDb.TransactionModel();

                var filter = Builders<Customer>.Filter.Eq("accountNumber", accountNumber);
                var receipient = customerCollection.FindSync<Customer>(filter).FirstOrDefault();

                var secondFilter = Builders<Customer>.Filter.Eq("username", customer.username);


                if (receipient == null)
                {
                    Console.WriteLine("Receipient Account Not found");
                    transfer(customer);
                }

                Transaction transaction = new Transaction()
                {
                    amount = amount,
                    receiverAccountNumber = receipient.accountNumber,
                    receiverName = receipient.accountName,
                    senderAccountNumber = customer.accountNumber,
                    senderName = customer.accountName,
                    transactionType = Enums.TransactionType.TRANSFER
                    
                };

                //add transaction to both parties 

                customer.accountBalance -= amount;
                receipient.accountBalance += amount;

                customer.transactions.Add(transaction);
                receipient.transactions.Add(transaction);

                customerCollection.FindOneAndReplaceAsync(filter, receipient);
                customerCollection.FindOneAndReplaceAsync(secondFilter, customer);
                transactionCollection.InsertOneAsync(transaction);


                Console.WriteLine("Transfer Successful...");

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                transfer(customer);
            }  
        }

        public void withdraw(Customer customer)
        {
            Console.WriteLine("Enter Amount");
            try
            {
                var amount = Convert.ToDouble(Console.ReadLine());

                if (customer.accountBalance < amount)
                {
                    Console.WriteLine("Insuffuent funds");
                    return;
                }else
                {
                    Transaction transaction = new Transaction();
                    transaction.amount = amount;
                    transaction.transactionType = Enums.TransactionType.WITHDRAW;
                    transaction.receiverName = customer.accountName;
                    transaction.receiverAccountNumber = customer.accountNumber;


                    customer.accountBalance -= amount;
                    customer.transactions.Add(transaction);

                    var transactionCollection = MongoDb.TransactionModel();
                    var customerCollection = MongoDb.CustomerModel();
                    var filter = Builders<Customer>.Filter.Eq("username", customer.username);
                    transactionCollection.InsertOneAsync(transaction);
                    customerCollection.FindOneAndReplace(filter, customer);

                    Console.WriteLine($"You have withdrawn {"$" + amount}");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                withdraw(customer);
            }
        }
    }
}
