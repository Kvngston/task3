﻿using MongoDB.Bson;
using System;
using Task3.Enums;

namespace Task3.Models
{
    public class Transaction
    {

        public ObjectId _id { get; set; }
        public string senderName { get; set; }
        public string receiverName { get; set; }
        public double amount { get; set; }
        public string senderAccountNumber { get; set; }
        public string receiverAccountNumber { get; set; }
        public TransactionType transactionType { get; set; }
        public DateTime createdAt { get; set; }


        public Transaction()
        {
            createdAt = DateTime.Now;
            _id = ObjectId.GenerateNewId();
        }


        public override string ToString()
        {
            switch (transactionType)
            {
                case TransactionType.DEPOSIT:
                    {
                        return $"Deposit Transaction of amount ${amount}, made by {receiverName} with account number {receiverAccountNumber}";
                    }

                case TransactionType.WITHDRAW:
                    {
                        return $"Withdrawal Transaction of amount ${amount}, made by {receiverName} with account number {receiverAccountNumber}";
                    }
                case TransactionType.TRANSFER:
                    {
                        return $"Transfer Transaction of amount ${amount}, made by {senderName} with account number {senderAccountNumber} to {receiverName} with account number {receiverAccountNumber}";
                    }

                default:
                    {
                        return this.ToJson();
                    }
            }
        }
    }

}