﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task3.Enums;

namespace Task3.Models
{
    public class Customer
    {
        public ObjectId _id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public int age { get; set; }
        public string accountNumber { get; set; }
        public string accountName { get; set; }
        public double accountBalance { get; set; }
        public List<Transaction> transactions { get; set; }


        public Customer()
        {
            Random random = new Random();
            var generated = random.Next(999999999);
            this._id = ObjectId.GenerateNewId();
            this.accountNumber = generated.ToString();
            this.accountBalance = 0;
            this.transactions = new List<Transaction>();
        }


        public override string ToString()
        {
            return $"Customer Account Name is {accountName}";
        }

        public void userTransactions()
        {

            transactions.ForEach(transaction => {

                switch (transaction.transactionType)
                {
                    case TransactionType.DEPOSIT:
                        {
                            Console.WriteLine($"Deposit Transaction of amount ${transaction.amount}, made by {transaction.receiverName} with account number {transaction.receiverAccountNumber}");
                            break;
                        }

                    case TransactionType.WITHDRAW:
                        {
                            Console.WriteLine($"Withdrawal Transaction of amount ${transaction.amount}, made by {transaction.receiverName} with account number {transaction.receiverAccountNumber}");
                            break;
                        }
                    case TransactionType.TRANSFER:
                        {
                            Console.WriteLine($"Transfer Transaction of amount ${transaction.amount}, made by {transaction.senderName} with account number {transaction.senderAccountNumber} to {transaction.receiverName} with account number {transaction.receiverAccountNumber}");
                            break;
                        }

                    default:
                        {
                            Console.WriteLine(this.ToJson());
                            break;
                        }
                }

            });
        }
    }
}
 